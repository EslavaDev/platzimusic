/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, { Component } from "react";
import { StyleSheet, Platform } from "react-native";
import { Scene, Router, Stack, Overlay } from 'react-native-router-flux';
import HomeView from './src/HomeView';
import ArtistDetail from './src/ArtistDetail';
import LoginView from './src/LoginView';

export default class App extends Component {
  render() {
    // const isAndroid = Platform.OS === 'android';
    return (
      <Router>
        <Stack key="root">
          <Scene key="login" component={LoginView} hideNavBar={true}/>
          <Scene key="home" component={HomeView} hideNavBar={true} />
          <Scene key="detail" component={ArtistDetail} title="Comentarios"/>
        </Stack>
      </Router>
    );
  }
}
