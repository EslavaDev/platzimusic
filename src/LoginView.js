/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, { Component } from "react";
import {
  StyleSheet,
  View,
  Text,
  Image,
  AsyncStorage,
  ImageBackground
} from "react-native";
import { LoginButton, AccessToken, LoginManager } from "react-native-fbsdk";
import { Actions } from "react-native-router-flux";
import firebase from "react-native-firebase";
import back from './background.jpg';

export default class LoginView extends Component {

  state = {
    credentials: null
  }

  initUser = token => {
    fetch(
      "https://graph.facebook.com/v2.5/me?fields=email,name,friends&access_token=" +
        token
    )
      .then(response => response.json())
      .then(json => console.log(json))
      .catch(error => {
        console.error("ERROR GETTING DATA FROM FACEBOOK", error);
      });
  };

  componentWillMount = () => {
    this.authenticateUser();
  };
  authenticateUser = async () => {
    try {
     const data = await AccessToken.getCurrentAccessToken();
      if (!data) {
        throw new Error(
          "Something went wrong obtaining the users access token"
        ); // Handle this however fits the flow of your app
      }
      const { accessToken } = data;
      // create a new firebase credential with the token
      const credential = firebase.auth.FacebookAuthProvider.credential(
        accessToken
      );
      // login with credential
      const currentUser = await firebase
        .auth()
        .signInWithCredential(credential);
        console.info(JSON.stringify(currentUser.user.toJSON()));
        Actions.home()
    } catch (e) {
      console.error(e);
    }
  };

  render() {
    return (
      <ImageBackground source={require('./background.jpg')} style={styles.container}>
          <Image source={require('./logo.png')} style={styles.logo}/>
          <Text style={styles.welcome}>Bienvenidos a EslavaDevMusic</Text>  
        <LoginButton
          readPermissions={["public_profile", "email", "user_friends"]}
          onLoginFinished={async (error, result) => {
            try {
              if (error) {
                console.log("login has error: " + result.error);
              } else if (result.isCancelled) {
                console.log("login is cancelled.");
              } else {
                this.authenticateUser();
              }
            } catch (e) {
              console.error(e);
            }
          }}
          onLogoutFinished={() => console.log("logout.")}
        />
      </ImageBackground>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "lightgray",
    justifyContent: "center",
    alignItems: "center"
  },
  logo: {
    width: 150,
    height: 150,
    marginBottom: 15,
  },
  welcome: {
    fontSize: 24,
    fontWeight: "600",
    marginBottom: 20
  }
});
