/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, {Component} from 'react';
import { StyleSheet, FlatList, TouchableOpacity } from 'react-native';
import PropTypes from 'prop-types';
import ArtistBox from './ArtistBox';
import { Actions } from 'react-native-router-flux';

export default class ArtisList extends Component {

  handlePress = (artist) => {
    Actions.detail({artist});
  }
  render() {
    return (
      //FlatList
      <FlatList
      data={this.props.artists}
      renderItem={({item, i}) => {
        return (
          <TouchableOpacity
            onPress={()=> this.handlePress(item)}
          >
            <ArtistBox artist={item}/>
          </TouchableOpacity>
        )}
      }
      keyExtractor={item => item.index || item.key}
      />
    );
  }
}

ArtisList.propTypes = {
    artists: PropTypes.array.isRequired
  };
  
ArtisList.defaultProps = {
    artists: []
  };

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'lightgray',
    paddingTop: 50,
  }
});
