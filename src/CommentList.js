import React, {Component} from 'react';
import { StyleSheet, FlatList } from 'react-native';
import PropTypes from 'prop-types';
import Comment from './Comment';


export default class CommentList extends Component {

    state= {
        data: []
    }

    componentWillReceiveProps = (newProps) => {
        
        const data = newProps.comments.map((d, i) => ({...d, key: i.toString()})).reverse();
        this.setState({data})
    }
  render() {
    return (
      //FlatList
      <FlatList
      data={this.state.data}
      renderItem={({item, i}) => {
        return (     
            <Comment text={item.text} avatar={item.userPhoto}/>
        )}
      }
      keyExtractor={item => item.index || item.key}
      />
    );
  }
}

CommentList.propTypes = {
    comments: PropTypes.array.isRequired
  };
  
CommentList.defaultProps = {
    comments: []
  };

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'lightgray',
    paddingTop: 50,
  }
});
