
const URL = 'http://ws.audioscrobbler.com/2.0/?method=geo.gettopartists&country=colombia&api_key=91ba2ce30e9432e56b00feef33ab813c&format=json'

export const getArtists = () => fetch(URL)
                            .then(response => response.json())
                            .then(data => data.topartists.artist)
                            .then(artists => artists.map( (artist, i) => ({
                                id: artist.mbid,
                                key: i.toString(),
                                name: artist.name,
                                image: artist.image[3]['#text'],
                                comments: Math.floor(Math.random()*1000),
                                likes: Math.floor(Math.random()*1000)
                                })))
                            .catch(err => console.warn(err))

