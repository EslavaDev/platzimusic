
import React, {Component} from 'react';
import { StyleSheet, View, TextInput, TouchableOpacity, Text } from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';
import firebase from 'react-native-firebase';
import ArtistBox from './ArtistBox';
import CommentList from './CommentList';

export default class ArtistDetail extends Component {
  state = {
    text: null,
    comments: []
  }
 
  componentDidMount = () => {
    this.getArtistComentRef().on('child_added', this.addComents)
  }

  componentWillUnmount = () => {
    this.getArtistComentRef().off('child_added', this.addComents)
  }

  addComents = data => {
    const comment = data.val();
    const { comments } = this.state;
    this.setState({
      comments: comments.concat(comment)
    });
  }

  handleChangeText = text => this.setState({text})

  handleSend = () => {
    const { photoURL,uid } = firebase.auth().currentUser;
    const { text } = this.state;
    const artistComentRef = this.getArtistComentRef();
    const newComment = artistComentRef.push();
    newComment.set({
      text,
      userPhoto: photoURL,
      uid
    });
    this.setState({text:''})
  }

  getArtistComentRef = () => {
    const { id } = this.props.artist
    return firebase.database().ref(`comments/${id}`)
  }
  render() {
    const artist = this.props.artist;
    const { comments } = this.state;
    return (
      <View style={styles.container}>
        <ArtistBox artist={artist}/>
        <CommentList comments={comments}/>
        <View style={styles.inputContainer}>
          <TextInput 
            style={styles.input}
            value={this.state.text}
            placeholder="Escribe un comentario aqui"
            onChangeText={this.handleChangeText}
          />
          <TouchableOpacity onPress={this.handleSend}>
            <Icon name="ios-send" size={30} color="gray" />
          </TouchableOpacity>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'lightgray',
    paddingTop: 10,
    
  },
  inputContainer: {
    height: 50,
    paddingHorizontal: 10,
    backgroundColor: 'white',
    flexDirection: 'row',
    alignItems: 'center'
  },
  input: {
    flex: 1,
    height: 50
  },
});
