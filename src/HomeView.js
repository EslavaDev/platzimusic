/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, {Component} from 'react';
import { StyleSheet, View, ActivityIndicator, Platform } from 'react-native';
import ArtisList from './ArtisList';
import { getArtists } from './api-client';

export default class HomeView extends Component {

  state={
    artists: null
  }

  componentDidMount = () => {
    getArtists()
    .then(data => this.setState({ artists: data}))
    .catch(err => console.log(err))
  }
  render() {
    const artists = this.state.artists
    return (
      <View style={styles.container}>
      { !artists
        ? <ActivityIndicator size="large"/>
        : <ArtisList artists={artists}/>
      }
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'lightgray',

    paddingTop: Platform.select({
      ios: 30,
      android: 10
    }),
  }
});
