import React from 'react';
import { StyleSheet, Text, View, Image } from 'react-native';
import PropTypes from 'prop-types';

const Comment = (props) => {

    return (
        <View style={styles.comment}>
            <Image style={styles.avatar} source={{uri: props.avatar}} />
            <Text style={styles.text}>{props.text}</Text>
        </View>
    )
}

Comment.propTypes = {
    text: PropTypes.string.isRequired,
    avatar: PropTypes.string
  };
  
Comment.defaultProps = {
    text: '',
    avatar: 'http://s3.amazonaws.com/37assets/svn/765-default-avatar.png'
  };

const styles = StyleSheet.create({
    comment: {
        backgroundColor: '#ecf0f1',
        padding: 10,
        margin: 5,
        borderRadius: 5,
        flexDirection: 'row',
        alignItems: 'center',
    },
    text: {
        fontSize: 16
    },
    avatar: {
        width: 32,
        borderRadius: 16,
        marginRight: 10,
        height: 32,
    }
})
export default Comment;