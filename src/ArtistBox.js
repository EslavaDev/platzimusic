/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, {Component} from 'react';
import {TouchableOpacity, StyleSheet, Image, Text, View, Alert} from 'react-native';
import PropTypes from 'prop-types';
import Icon from 'react-native-vector-icons/FontAwesome';
import firebase from 'react-native-firebase';
export default class ArtistBox extends Component {

  state = {
    liked: false,
    likeCount: 0,
  }

  componentWillMount = () => {
    const { uid } = firebase.auth().currentUser;
    this.getArtistsRef().on('value', snapshot => {
      const artist = snapshot.val();
      if (artist) {
        this.setState({
          likeCount: artist.likeCount,
          liked: artist.likes && artist.likes[uid]
        })
      }
    })
  }

  handleLikesPress = () => {
    const { liked } = this.state;
    this.setState({liked: !liked});
    this.toggleLiked(!liked)

  }
  getArtistsRef = () => {
    const { id } = this.props.artist
    return firebase.database().ref(`artist/${id}`)
  }
  toggleLiked = (liked) => {
    const { uid } = firebase.auth().currentUser;
    this.getArtistsRef().transaction(artist => {
      if(artist) {
        if (artist.likes && artist.likes[uid]) {
          artist.likeCount--;
          artist.likes[uid] = null;
        } else {
          artist.likeCount++;
          if (!artist.likes) {
            artist.likes = {};
          }
          artist.likes[uid] = true;
        }
      }
      return artist || {
        likeCount: 1,
        likes: {
          [uid]: true
        }
      };
    })
  }
  render() {
    const { image, name, comments } = this.props.artist;
    const { likeCount } = this.state;
    const icon = (this.state.liked)
     ? <Icon name="heart" size={30} color="#e74c3c" />
     : <Icon name="heart-o" size={30} color="gray" />
    
    return (
        <View style={styles.artistBox}>
          <Image style={styles.image} source={{ uri: image }} />
          <View style={styles.info}>
            <Text style={styles.name}>{name}</Text>
            <View style={styles.row}>
              <View style={styles.iconContainer}>
                <TouchableOpacity onPress={this.handleLikesPress}>
                  {icon}
                </TouchableOpacity>
                <Text style={styles.count}>{likeCount}</Text>
              </View>
              <View style={styles.iconContainer}>
                <TouchableOpacity>
                  <Icon name="comments-o" size={30} color="gray" />
                </TouchableOpacity>
                <Text style={styles.count}>{comments}</Text>
              </View>
            </View>
          </View>
        </View>
    );
  }
}

ArtistBox.propTypes = {
  artist: PropTypes.object.isRequired
};

ArtistBox.defaultProps = {
  artist: {
    image: '', name:'', likes:'', comments:''
  }
};

const styles = StyleSheet.create({
  image: {
    width: 150,
    height: 150,
  },
  artistBox : {
    margin: 5,
    flexDirection: 'row',
    backgroundColor: 'white',
    shadowColor: 'black',
    shadowOpacity: .2,
    shadowOffset: {
      height: 1,
      width: -2

    },
    elevation: 2
  },
  info: {
    flex: 1,
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'center',
  },
  name: {
    fontSize: 20,
    marginTop:  10,
    color: '#333'
  },
  row: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginHorizontal: 40,
    marginTop: 15,
  },
  iconContainer : {
    flex: 1,
    alignItems: 'center',
  },
  count: {
    color: "gray"
  }
});
